(define add (lambda (a b c d) (a c (b c d)))) ;addition function

(define logicor (lambda (m n) (lambda (a b) (n a(m a b))))) ;OR function

(define iszero (lambda (n) n (lambda (x) (false)) true)) ;iszero function

(define logicand (lambda (m n) (lambda (a b) (n (m a b) b)))) ;AND function

(define logicnot (lambda (m) (lambda (a b) (m b a)))) ;NOT function

(define LEQ (lambda (m n)(iszero (sub m n)))) ;LEQ function

(define EQ (lambda (m n) (logicand (LEQ m n) (LEQ n m)))) ;EQ function

(define LT (lambda (m n) (logicand (LEQ m n) (logicnot(EQ m n))))) ;LT function

(define Y (lambda (f) ((lambda (x) (f (x x))) (lambda (x) (f (x x)))))) ;Y combinator function

(define recursive (lambda (f n) (iszero n) 0 (add (f n-1) n))) ;recursive function inside if

(define sumofnintegers (lambda (m n) (logicor(iszero m)(LT n m)) (f(Y recursive) (add m n)))) ;if-else